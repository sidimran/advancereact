import React, { Fragment, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

import useDocumentTitle from "./useDocumentTitle";

function Counter(props) {
  const [count, setState] = useState(0);
  const [name, setName] = useState("");

  useDocumentTitle(`${name}has clicked ${count} times!`);

  return (
    <Fragment>
      <input type="text" onChange={(e) => setName(e.target.value)} />
      <div>
        {" "}
        {name}has clicked {count}times! :{count}
      </div>
      <button className="btn btn-primary" onClick={() => setState(count + 1)}>
        Increase
      </button>
    </Fragment>
  );
}

export default Counter;
