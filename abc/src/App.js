// import Movie from "./hoc/Movie";
// import React from "react";
// import "./App.css";
//  import Counter from "./Hooks/Counter";

// import Users from "./Hooks/Users";

// function App(props) {
//   return (
//     <>
//       {/* <Counter />
//       <Users /> */}
//     </>
//   );
// }

// export default App;

import React, { Component } from "react";
import Login from "./context/Login";
import MoviePage from "./context/MoviePage";
import UserContext from "./context/userContext";

import CartContext from "./context/cartContext";

class App extends Component {
  handleLoggedIn = (username) => {
    console.log("Getting the user " + username);
    const user = { name: "syed" };
    this.setState({ currentUser: user });
  };
  // state = {
  //   currentuser: {
  //     name: "syed",
  //   },
  // };

  state = {
    currentUser: null,
  };
  render() {
    return (
      // <div>
      //   <MoviePage />
      // </div>

      <CartContext.Provider value={{cart :[]}}>
        <UserContext.Provider
          value={{
            currentUser: this.state.currentUser,
            onLoggedIn: this.handleLoggedIn,
          }}
        >
          <div>
            <MoviePage />
            <Login />
          </div>
        </UserContext.Provider>
      </CartContext.Provider>
    );
  }
}
export default App;
