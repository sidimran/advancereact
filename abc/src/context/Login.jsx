import React, { useContext } from "react";
import UserContext from "./userContext";
import 'bootstrap/dist/css/bootstrap.min.css';

function Login(props) {
  const userContext = useContext(UserContext);
  return (
    <div>
      <button
        onClick={() => userContext.onLoggedIn("username")}
        className="btn btn-primary"
      >Click</button>
    </div>
  );
}

export default Login;
